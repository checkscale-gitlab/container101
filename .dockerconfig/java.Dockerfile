#Base Images
FROM openjdk:7

#Work Directory
WORKDIR /usr/src/myapp

#Copy code
COPY java /usr/src/myapp/

#Compile Java File
RUN javac Main.java

#RUN Java
CMD ["java", "Main"]


